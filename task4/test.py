import pandas as pd
import numpy as np
from tensorflow.keras import layers
from tensorflow.keras import Model
from tensorflow.keras import optimizers
from efficientnet.tfkeras import EfficientNetB3
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from skimage import transform
import tensorflow as tf
import tensorflow.keras.backend as K

device_name = tf.test.gpu_device_name()
print(device_name)
print("hello")
'''
Task: Learn image similarity given a set of triplets consisting of anchor, positive, and negative.
Our solution: We use a three-fold siamese CNN and triplet loss to learn the similarity.
Inspired by Jiang Wang et al., 'Learning Fine-grained Image Similarity with Deep Ranking'
'''
# set random seed
tf.random.set_seed(653)

# check if we are computing on gpu
# tf.debugging.set_log_device_placement(True)

#############
# FUNCTIONS #
#############
'''
Returns the base model to be shared that learns the embedding.
'''
def base_model(input_dim):

    pretrained_net = EfficientNetB3(
        input_shape=input_dim,
        weights='imagenet',
        include_top=False
    )

    # transfer learning using a pre trained model
    for layer in pretrained_net.layers:
        layer.trainable = False
    for layer in pretrained_net.layers[-8:]:
        layer.trainable = True

    x = layers.Flatten()(pretrained_net.output)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.2)(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.2)(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.2)(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Lambda(lambda y: tf.math.l2_normalize(y, axis=1))(x)

    model = Model(inputs=pretrained_net.input, outputs=x)
    return model


'''
Loads the specified image and pre processes it slightly.
'''
def read_image(food_id, dimension):
    img = load_img(IMAGES.format(str(food_id).zfill(5)))
    img = img_to_array(img).astype("float64")
    img = transform.resize(img, dimension)
    img *= 1. / 255

    return img


'''
Generator for the triplets.
'''
def triplet_generator(triplets, batch_size, img_dim):
    # dummy labels
    labels = np.ones(batch_size)

    batch_count = 0
    arr_anc = []
    arr_pos = []
    arr_neg = []

    index = 0
    length = triplets.shape[0]

    while True:
        batch_count = batch_count + 1

        row = triplets.iloc[index % length]
        index = index + 1

        anchor = row['anchor']
        positive = row['positive']
        negative = row['negative']

        arr_anc.append(read_image(anchor, img_dim))
        arr_pos.append(read_image(positive, img_dim))
        arr_neg.append(read_image(negative, img_dim))

        if batch_count == batch_size:
            arr_anc = np.array(arr_anc)
            arr_pos = np.array(arr_pos)
            arr_neg = np.array(arr_neg)
            yield [arr_anc, arr_pos, arr_neg], labels
            arr_anc = []
            arr_pos = []
            arr_neg = []
            batch_count = 0


'''
Triplet loss used for training.
Source: https://medium.com/@prabhnoor0212/siamese-network-keras-31a3a8f37d04
'''
def triplet_loss(y_true, y_pred, alpha=0.5):
    total_length = y_pred.shape.as_list()[-1]

    anchor = y_pred[:, 0:int(total_length * 1 / 3)]
    positive = y_pred[:, int(total_length * 1 / 3):int(total_length * 2 / 3)]
    negative = y_pred[:, int(total_length * 2 / 3):int(total_length * 3 / 3)]

    # distance between the anchor and the positive
    pos_dist = K.sum(K.square(anchor - positive), axis=1)

    # distance between the anchor and the negative
    neg_dist = K.sum(K.square(anchor - negative), axis=1)

    # compute loss
    basic_loss = pos_dist - neg_dist + alpha
    loss = K.maximum(basic_loss, 0.0)

    return loss


'''
Predicts if the given triplets are of the form A,P,N.
'''
def predict_labels(predictor, triplets, f, img_dim):
    answ = []

    for (index, row) in triplets.iterrows():

        anchor = row['anchor']
        positive = row['positive']
        negative = row['negative']

        image_a = np.array([read_image(anchor, img_dim)])
        image_p = np.array([read_image(positive, img_dim)])
        image_n = np.array([read_image(negative, img_dim)])

        pred_a = predictor.predict(image_a)
        pred_p = predictor.predict(image_p)
        pred_n = predictor.predict(image_n)

        # distance between the anchor and the positive
        dist_p = K.sum(K.square(pred_a - pred_p), axis=1)

        # distance between the anchor and the negative
        dist_n = K.sum(K.square(pred_a - pred_n), axis=1)

        if dist_p < dist_n:
            f.write('{}\n'.format(1))
            answ.append(1)
        else:
            f.write('{}\n'.format(0))
            answ.append(0)

    return answ


########
# MAIN #
########

# global variables
TRAINING = True  # boolean indicating if we want to freshly train the model

# path to the weights of different models
IMAGES = 'food/{}.jpg'
PLOT = 'plots/final_model.png'
OUR_WEIGHTS = 'models/my_model.h5'
INCEPTION_WEIGHTS = 'models/inception_v3_weights_tf_dim_ordering_tf_kernels_notop.h5'

# prepare needed files
output = open('predictions.txt', 'w+')  # file for the predictions

# input dimension of the siamese net
feature_dim = (224, 224, 3)

# read and prepare the train triplets
# contains the train triplets where anchor is supposed to be close to positive and farther from negative
train_triplets = pd.read_csv('train_triplets.txt', sep=' ', dtype=int, header=None)
train_triplets.columns = ['anchor', 'positive', 'negative']

# read and prepare the test triplets
test_triplets = pd.read_csv('test_triplets.txt', sep=' ', dtype=int, header=None)
test_triplets.columns = ['anchor', 'positive', 'negative']

# select a subset of the triplets
# train_triplets = train_triplets.iloc[0:100, :]
# test_triplets = test_triplets.iloc[0:100, :]

# prepare the model
base_net = base_model(feature_dim)

input_a = layers.Input(shape=feature_dim)
input_b = layers.Input(shape=feature_dim)
input_c = layers.Input(shape=feature_dim)

# because we re-use the same instance base_net the weights of the network will be shared across the branches
anchor_model = base_net(input_a)
positive_model = base_net(input_b)
negative_model = base_net(input_c)

# the model we use for the prediction
test_model = Model(input_a, anchor_model)

# the model we use for training
merged_layers = layers.concatenate([anchor_model, positive_model, negative_model])
final_model = Model(inputs=[input_a, input_b, input_c], outputs=merged_layers)

# summarize and plot the model
base_net.summary()
final_model.summary()
# plot_model(
#     base_net,
#     to_file=PLOT,
#     show_shapes=False,
#     show_layer_names=True,
#     rankdir="TB",
#     expand_nested=False,
#     dpi=96,
# )


# train the model and store the weights
if TRAINING:  # we want to train the model
    print('START: started training the model')

    learning_rate = 0.0001

    final_model.compile(
        loss=triplet_loss,
        optimizer=optimizers.Adam(learning_rate=learning_rate)
    )

    # define parameters
    epochs = 2
    batch_size = 64
    steps_per_epoch = train_triplets.shape[0] // batch_size

    final_model.fit(
        x=triplet_generator(train_triplets, batch_size, (feature_dim[0], feature_dim[1])),
        epochs=epochs,
        batch_size=batch_size,
        steps_per_epoch=steps_per_epoch,
        validation_data=None
    )

    final_model.save_weights(OUR_WEIGHTS)
    print('DONE: wrote model to file')
else:  # we want to load the model from stored weights
    final_model.load_weights(OUR_WEIGHTS)

# prepare the test generator

# predict the labels
# prediction = predict_labels(test_model, X_train, output)
prediction = predict_labels(test_model, test_triplets, output, (feature_dim[0], feature_dim[1]))
# prediction = predict_labels(test_model, train_triplets, output, (feature_dim[0], feature_dim[1]))

# print the predictions and wrap-up
print(prediction)
print(prediction.count(1))

output.close()