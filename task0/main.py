import pandas
import numpy
from sklearn.metrics import mean_squared_error
from sklearn import linear_model


def calcError(ix, py):
    y = []
    for i in ix:
        y.append(sum(i) / 10)
    return mean_squared_error(y, py) ** 0.5


if __name__ == '__main__':
    # Load Data

    # Train model
    train_dataset = pandas.read_csv("train.csv")
    y = train_dataset.iloc[1:2975, 1].to_numpy()
    x = train_dataset.iloc[1:2975, 2:12].to_numpy()
    model = linear_model.LinearRegression()
    model.fit(x, y)

    # Get model prediction
    test_dataset = pandas.read_csv("test.csv")
    x = test_dataset.iloc[:, 1:11].to_numpy()
    pred_y = model.predict(x)

    # Print error
    #print(calcError(x,pred_y))

    # Write prediction to prediction.csv
    prediction = pandas.DataFrame(test_dataset.iloc[:, 0])
    prediction['y'] = pred_y
    prediction.to_csv("prediction.csv", index=False)
