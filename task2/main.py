import pandas as pd
import numpy as np
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.kernel_ridge import KernelRidge
from joblib import dump, load
from sklearn import svm
#from score_submission import get_score
import sklearn.metrics as metrics

headers = ['pid', 'LABEL_BaseExcess', 'LABEL_Fibrinogen', 'LABEL_AST', 'LABEL_Alkalinephos', 'LABEL_Bilirubin_total',
           'LABEL_Lactate', 'LABEL_TroponinI', 'LABEL_SaO2', 'LABEL_Bilirubin_direct', 'LABEL_EtCO2',
           'LABEL_Sepsis',
           'LABEL_RRate', 'LABEL_ABPm', 'LABEL_SpO2', 'LABEL_Heartrate']


def load_train_x(filename):
    data = pd.read_csv(filename)
    pid = data.iloc[:, 0]
    age = data.iloc[:, 2]
    data = data.iloc[:, 3:]

    nan_value = data.iloc[0, 1]
    #del data["Time"]
    cols = len(data.columns)
    for i in range(1, cols):
        
        temp = data.iloc[:, i]
        #print(np.nanmean(temp.to_numpy()))
        temp = temp.replace([nan_value], np.nanmean(temp.to_numpy())) #for mean
        #temp = temp.replace([nan_value], np.nanmedian(temp.to_numpy())) #for median
        #print(temp)
        data.iloc[:, i] = temp
        #print(temp)

    #print(data)
    #data = data.replace([data.iloc[0, 1]],0 )
    rows = len(data)
    train_data = pd.DataFrame(columns=list(range(410)))

    for i in range(int(rows / 12)):
        rows = (data.iloc[(12 * i): (12 * i) + 12, :]).to_numpy()
        temp = np.append([], rows)
        temp = np.insert(temp, 0, pid[i*12])
        temp = np.insert(temp, 1, age[i*12])
        train_data = np.vstack((train_data, temp))

    return train_data


def load_train_y(filename):
    data = pd.read_csv(filename)
    return data.to_numpy()


if __name__ == '__main__':
    #train_data_y = load_train_y()
    train_data_x = load_train_x('train_features.csv')
    pd.DataFrame(train_data_x).to_csv('data_mean_age.csv')
    #train_data_x = pd.read_csv('data_with_mean.csv').to_numpy()
    #print(train_data_x)
    #train_data_x = pd.read_csv('test_features_comp.csv').to_numpy()
    #train_data_x = pd.read_csv('train_data_x.csv').to_numpy()
    #train_data_y = pd.read_csv('train_data_y.csv').to_numpy()
    #test_x = pd.read_csv('test_features_comp.csv').to_numpy()

    test_x = load_train_x('test_features.csv')
    pd.DataFrame(test_x).to_csv('data_mean_age_test.csv')
    #test_x = pd.read_csv('data_with_mean_test.csv').to_numpy()


    #res = pd.DataFrame(columns=headers)
    #res["pid"] = train_data_x[:1000, 0]
    #res = pd.read_csv('submission.zip', compression='zip')

    train_data_x = train_data_x[:, 2:]
    test_x = test_x[:, 2:]

    #df_true = pd.DataFrame(train_data_y[:1000, :])
    #df_true.columns = headers

# #Task 1
#
#     for i in range(10):
#         #y1 = train_data_y[:, i + 1]
#         #clf_2 = make_pipeline(StandardScaler(), svm.SVC(probability=True, cache_size=50000, degree=10, decision_function_shape='ovo', C=15.0))
#         #clf_2.fit(train_data_x, y1)
#
#         clf_2 = load('clf_' + str(i+1) + '.joblib')
#         ans = clf_2.predict_proba(train_data_x)
#         res[headers[i+1]] = ans[:,1]

# #Task 2
#     #clf_2 = make_pipeline(StandardScaler(),svm.SVC(probability=True, cache_size=50000, degree=10, decision_function_shape='ovo', C=15.0))
#     # clf_2.fit(train_data_x, y1)
#     sep_anal = load('clf_11.joblib')
#     ans = sep_anal.predict(train_data_x)
#
#     res['LABEL_Sepsis'] = ans

# Task 3
    """
    for i in range(11, 15):
        y1 = train_data_y[:, i + 1]
        clf_2 = make_pipeline(StandardScaler(), svm.SVR(cache_size=5000, C=0.5, kernel='rbf', coef0=1))
        clf_2.fit(train_data_x, y1)
        dump(clf_2, 'clf_mean_data_' + str(i+1) + '.joblib')

        #clf_2 = KernelRidge(alpha=20.0, kernel='sigmoid', degree=3, coef0=1, kernel_params=None, gamma=0.1)
        #clf_2.fit(train_data_x, y1)

        #clf_2 = load('clf_mean_data_' + str(i+1) + '.joblib')
        ans = clf_2.predict(test_x[:, :])
        res[headers[i+1]] = ans
    
    res.to_csv('submission.zip', index=False, float_format='%.3f', compression="zip")
    
    """
# # Testing
    #VITALS = ['LABEL_RRate', 'LABEL_ABPm', 'LABEL_SpO2', 'LABEL_Heartrate']
#   TESTS = ['LABEL_BaseExcess', 'LABEL_Fibrinogen', 'LABEL_AST', 'LABEL_Alkalinephos', 'LABEL_Bilirubin_total',
#           'LABEL_Lactate', 'LABEL_TroponinI', 'LABEL_SaO2',
#           'LABEL_Bilirubin_direct', 'LABEL_EtCO2']
# 
#   #get_score(df_true, res)
# 
#   task1 = np.mean([metrics.roc_auc_score(df_true[entry], res[entry]) for entry in TESTS])
#   task2 = metrics.roc_auc_score(df_true['LABEL_Sepsis'], res['LABEL_Sepsis'])
    #task3 = np.mean([0.5 + 0.5 * np.maximum(0, metrics.r2_score(df_true[entry], res[entry])) for entry in VITALS])
# 
    #print(task3)
#   score = np.mean([task1, task2, task3])
#   print(task1, task2, task3)
#   print(score)
