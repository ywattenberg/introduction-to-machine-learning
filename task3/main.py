import pandas as pd 
import numpy as np

from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.metrics import f1_score, recall_score, precision_score, confusion_matrix, log_loss
from sklearn.linear_model import LogisticRegression

from joblib import dump, load


if  __name__ == '__main__':

    raw_train_data = pd.read_csv('train.csv')
    raw_test_data = pd.read_csv('test.csv')

    train_y = raw_train_data.iloc[:, 1]

    switcher = {
    'R': 0,
    'H': 1,
    'K': 2,
    'D': 3,
    'E': 4,
    'S': 5,
    'T': 6,
    'N': 7,
    'Q': 8,
    'C': 9,
    'U': 10,
    'G': 11,
    'P': 12,
    'A': 13,
    'I': 14,
    'L': 15,
    'M': 16,
    'F': 17,
    'W': 18,
    'Y': 19,
    'V': 20,
    }


    train_x = np.zeros(84)
    
    for row in raw_train_data.iloc[:, 0]:
        temp = np.array([])
        for i in range(4):
            k = np.zeros(21)
            k[switcher.get(row[i])] = 1
            temp = np.append(temp, k)
        train_x = np.vstack((train_x, temp))
    
    train_x = train_x[1:, :]

    svm = make_pipeline(StandardScaler(), svm.SVC(cache_size=5000, kernel='rbf', probability=True))
    print("starting to train")
    svm.fit(train_x, train_y)
    dump(svm, 'final_svm.joblib')

    #svm = load('svm_balanced_4.joblib')

    test_x = np.zeros(84)

    for row in raw_test_data.iloc[:, 0]:
        temp = np.array([])
        for i in range(4):
            k = np.zeros(21)
            k[switcher.get(row[i])] = 1
            temp = np.append(temp, k)
        test_x = np.vstack((test_x, temp))
    
    test_x = test_x[1:, :]

    #svm = load('svm_21_v.joblib')
    print("starting to predict")
    prob = svm.predict_proba(test_x)

    threshhold = 0.38; #determined through testing with the train-set (best F1-score for train-set)
    y_pred = np.where(prob[:,1]> threshhold ,1,0)
    y_pred = pd.DataFrame(y_pred).iloc[1:,:]
    y_pred.to_csv('final_sol.csv', index=False)

