from sklearn import kernel_ridge
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
import pandas as pd
import numpy as np


def calc_kernel(arr):
    res = []
    for line in arr:
        new_line = np.array(line)
        new_line = np.append(new_line, line**2)
        new_line = np.append(new_line, np.exp(line))
        new_line = np.append(new_line, np.cos(line))
        new_line = np.append(new_line, 1)
        res.append(new_line)
    return np.array(res)


if __name__ == '__main__':
    # Load training data
    train_dataset = pd.read_csv("train.csv")
    y = train_dataset.iloc[:, 1].to_numpy()
    x = train_dataset.iloc[:, 2:7]
    x = x.to_numpy()
    new_x = calc_kernel(x)
    model = linear_model.Ridge(alpha=0.01)
    model.fit(new_x, y)
    print(model.coef_)
    weights = pd.DataFrame(model.coef_)
    weights.to_csv("weights.csv", index=False)

