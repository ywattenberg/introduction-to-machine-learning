First we load the training set into a Dataframe with pandas and then convert it to a numpy array to work with it.
We wrote a secondary function which applies the given functions on our inputs and appends them on a line of our numpy array.
We apply that function for each line of our input data and then get a new array of inputs which has 21 columns.
Then we use a linear model with ridge regression and alpha coefficient 0.01. The coefficient was determined manually to fit the 
expected model. Then we fit the model to our data and fetch the weights with the function model.coef_.
